# Projet Kubernetes pour l'Application Web

Ce dépôt contient les fichiers de configuration YAML nécessaires pour déployer et gérer une application web utilisant Kubernetes. L'application est divisée en plusieurs composants, dont le backend, le frontend et la base de données MySQL.

## Contenu

- `backend-deployment.yaml`: Fichier de configuration pour le déploiement du backend Spring Boot.
- `backend-service.yaml`: Fichier de configuration pour le service exposant le backend.
- `frontend-deployment.yaml`: Fichier de configuration pour le déploiement du frontend Angular.
- `frontend-service.yaml`: Fichier de configuration pour le service exposant le frontend.
- `mysql-deployment.yaml`: Fichier de configuration pour le déploiement de la base de données MySQL.

## Instructions d'utilisation

1. avoir Minikube installé localement pour créer un cluster Kubernetes.
2.  namespace Kubernetes nommé "webapp" en utilisant la commande `kubectl create namespace webapp`.
3. `kubectl apply` pour déployer les composants dans le cluster :
   ```bash
   kubectl apply -f backend-deployment.yaml -n webapp
   kubectl apply -f backend-service.yaml -n webapp
   kubectl apply -f frontend-deployment.yaml -n webapp
   kubectl apply -f frontend-service.yaml -n webapp
   kubectl apply -f mysql-deployment.yaml -n webapp
